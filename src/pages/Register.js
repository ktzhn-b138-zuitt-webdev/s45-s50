import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Redirect } from 'react-router-dom'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){
	const {user} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	// State to determine whether the submit buttion is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	// Function to simulate user registration
	function registerUser(e){
		e.preventDefault();

		setEmail('');
		setPassword1('');
		setPassword2('');
		setFirstName('');
		setLastName('');
		setMobileNo('');

		fetch('http://localhost:4000/signup', {
		    method: 'POST',
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		    	firstName: firstName,
		    	lastName: lastName,
		    	mobileNo: mobileNo,
		        email: email,
		        password1: password1
		    })
		})

		Swal.fire({
                        title: "Registration Successful",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    })
	}

	useEffect(() => {
		// Validate to enable submit button when all fields are populated and both passwords match
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2))
		{
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password1, password2])

	return (
		if (user.id !== null) {
			<Redirect to="/courses" />
		}
		else if (user.find({email: reqBody.email})).then(result => {
		// Check if email exists in the Users collection, then allows when no user has the same email
		if(result.length !== 0){
					<Form onSubmit={(e) => registerUser(e)}>
			{/*Bind the input states via 2-way binding*/}
			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
			  <Form.Control 
			  	type="string" 
			  	placeholder="First Name" 
			  	value={firstName}
			  	onChange = { e => setFirstName(e.target.value)}
			  	required />
			</Form.Group>
			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
			  <Form.Control 
			  	type="string" 
			  	placeholder="Last Name" 
			  	value={lastName}
			  	onChange = { e => setLastName(e.target.value)}
			  	required />
			</Form.Group>
			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
			  <Form.Control 
			  	type="string" 
			  	placeholder="Mobile Number" 
			  	value={mobileNo}
			  	onChange = { e => setMobileNo(e.target.value)}
			  	required />
			</Form.Group>
			  <Form.Group controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email"
			    	value={email}
			    	onChange = { e => setEmail(e.target.value)}
			    	required 
			    />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password1}
			    	onChange={ e => setPassword1(e.target.value)}
			    	required />
			  </Form.Group>
			  <Form.Group controlId="password2">
			  	<Form.Label>Verify Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Verify Password" 
			    	value={password2}
			    	onChange={ e => setPassword2(e.target.value)}
			    	required />
			  </Form.Group>

			  
			{/* Conditionally render the submit button based on isActive state */}
				{ isActive ? 
					<Button variant="primary" type="submit" id="submitBtn">
					    Submit
					</Button>
					:
					<Button variant="danger" type="submit" id="submitBtn" disabled>
					  Submit
					</Button>
				 }
			  
			</Form>
		}
	})
		else {
					Swal.fire({
			                        title: "Duplicate email found",
			                        icon: "error",
			                        text: "Please provide a different email"
			                    })

					<Redirect to="/login" />
		}

	)

}
